package com.demoapp.alcodesonboard.fragments;

import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.repositories.MyNotesRepository;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class MyNoteDetailFragment extends Fragment {

    public static final String TAG = MyNoteDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.edittext_title)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private Long mMyNoteId = 0L;

    public MyNoteDetailFragment() {
    }

    public static MyNoteDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        MyNoteDetailFragment fragment = new MyNoteDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_note_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // TODO change menu label "Save" to "Create" for new note.
        try {
            //Only existing note had extra string
            String test = getActivity().getIntent().getExtras().toString();
        }
        catch (NullPointerException e){
            menu.getItem(0).setTitle("Create");
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_save) {
            // TODO show confirm dialog before continue.
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure?")
                    .setTitle("Save")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String title = mEditTextTitle.getText().toString();
                            String content = mEditTextContent.getText().toString();

                            // TODO check email and password is blank or not.
                            if(TextUtils.isEmpty(title) || TextUtils.isEmpty(content)){
                                Toast.makeText(getActivity(), "Please enter something....", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                // Save record and return to list.
                                MyNote myNote = new MyNote();
                                myNote.setTitle(title);
                                myNote.setContent(content);

                                MyNotesViewModel mViewModel = new MyNotesViewModel(getActivity().getApplication());

                                // TODO BAD practice, should move Database operations to Repository.
                                if (mMyNoteId > 0) {
                                    // Update record.
                                    myNote.setId(mMyNoteId);
                                    mViewModel.editNote(mMyNoteId, title, content);
                                   /* DatabaseHelper.getInstance(getActivity())
                                            .getMyNoteDao()
                                            .save(myNote);*/
                                } else {
                                    // Create record.
                                    mViewModel.addNote(title, content);
//                                    DatabaseHelper.getInstance(getActivity())
//                                            .getMyNoteDao()
//                                            .insert(myNote);
                                }

                                Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();

                                getActivity().setResult(MyNoteDetailActivity.RESULT_CONTENT_MODIFIED);
                                getActivity().finish();
                            }
                        }
                    })
                    .setNegativeButton("No",null)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        // TODO BAD practice, should move Database operations to Repository.
        MyNotesViewModel mViewModel = new MyNotesViewModel(getActivity().getApplication());
        if (mMyNoteId > 0) {
            MyNote myNote = mViewModel.getNote(mMyNoteId);
//            MyNote myNote = DatabaseHelper.getInstance(getActivity())
//                    .getMyNoteDao()
//                    .load(mMyNoteId);
            if (myNote != null) {
                mEditTextTitle.setText(myNote.getTitle());
                mEditTextContent.setText(myNote.getContent());
            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }
}
